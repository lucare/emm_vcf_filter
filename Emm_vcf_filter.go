package main

import (
	"fmt"
	"log"
	"os"
	"bufio"
	"strings"
	"regexp"
	"strconv"
)

func main() {	
	filter_vcf()
}

func filter_vcf() {
	fmt.Println("## Filtering vcf ...")
	
	// Open file in ...
	vcf_in_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.vcf"
	vcf_in_fh, err := os.Open(vcf_in_filename)
	if err != nil {
		log.Fatal(err)
	}
	defer vcf_in_fh.Close()
	fmt.Println("Opening: "+vcf_in_filename)

	// Open file out ...
	vcf_out_only_pro_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.only_proband_variants.vcf"
	vcf_out_only_pro_fh, err := os.Create(vcf_out_only_pro_filename)
	if err != nil {
		// handle the error here
		fmt.Println(err)
		return
	}
	fmt.Println("Creating: "+vcf_out_only_pro_filename)
	
	vcf_out_pro_hom_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.only_proband_variants.pro_hom.vcf"
	vcf_out_pro_hom_fh, err := os.Create(vcf_out_pro_hom_filename)
	if err != nil {
		// handle the error here
		fmt.Println(err)
		return
	}
	fmt.Println("Creating: "+vcf_out_pro_hom_filename)
	
	vcf_out_match_brother_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.only_proband_variants.pro_hom.match_brother.vcf"
	vcf_out_match_brother_fh, err := os.Create(vcf_out_match_brother_filename)
	if err != nil {
		// handle the error here
		fmt.Println(err)
		return
	}
	fmt.Println("Creating: "+vcf_out_match_brother_filename)
	
	vcf_out_not_kids_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.only_proband_variants.pro_hom.match_brother.not_match_kids.vcf"
	vcf_out_not_kids_fh, err := os.Create(vcf_out_not_kids_filename)
	if err != nil {
		// handle the error here
		fmt.Println(err)
		return
	}
	fmt.Println("Creating: "+vcf_out_not_kids_filename)

	vcf_out_filename := "Lane_Westhoff_13_samples_30x_PCRFree_WGS_08242020.vcfanno.only_proband_variants.pro_hom.match_brother.not_match_kids.below_freq.vcf"
	vcf_out_fh, err := os.Create(vcf_out_filename)
	if err != nil {
		// handle the error here
		fmt.Println(err)
		return
	}
	fmt.Println("Creating: "+vcf_out_filename)

	// Read file line by line ...
	scanner := bufio.NewScanner(vcf_in_fh)
	buf := make([]byte, 0, 64*1024)
	scanner.Buffer(buf, 1024*1024)
	re_gno_af_all := regexp.MustCompile(`gno_af_all=(.*?)(;|,)`)
	//var gt_fields [][]string
	vcf_line_counter := 0
	vcf_line_counter_proband_has_variant := 0
	vcf_line_counter_proband_hom := 0
	vcf_line_counter_proband_match_brother := 0
	vcf_line_counter_proband_not_match_kids := 0
	vcf_line_below_freq_counter := 0
	for scanner.Scan() {
		line := scanner.Text()
		if (line != "" && !(strings.HasPrefix(line, "#"))) { // not a comment, so it is a vcf record ...
			vcf_line_counter++
			//println("line: "+line)
			fields := strings.Split(line, "\t")
			
			gt_proband := strings.Split(fields[9], ":")[0]
			gt_brother := strings.Split(fields[13], ":")[0]
			gt_son := strings.Split(fields[12], ":")[0]
			gt_daughter := strings.Split(fields[11], ":")[0]
			
			//println("gt_proband: "+gt_proband)
			//println("gt_brother: "+gt_brother)
			//println("gt_son: "+gt_son)
			//println("gt_daughter: "+gt_daughter)
			gt_proband_1 := strings.Split(gt_proband, "/")[0]
			gt_proband_2 := strings.Split(gt_proband, "/")[1]
			gt_son_1 := strings.Split(gt_son, "/")[0]
			gt_son_2 := strings.Split(gt_son, "/")[1]
			gt_daughter_1 := strings.Split(gt_daughter, "/")[0]
			gt_daughter_2 := strings.Split(gt_daughter, "/")[1]
			
			if (gt_proband != "0/0" && gt_proband != "./.") {	// proband has variants ...
				vcf_line_counter_proband_has_variant++
				vcf_out_only_pro_fh.WriteString(line+"\n")
				if (gt_proband_1 != "0" && gt_proband_2 != "0" && gt_proband_1 == gt_proband_2) {	// proband has a homozygous alt (need to do this way to get 2/2, 3/3, etc ...
					vcf_line_counter_proband_hom++
					vcf_out_pro_hom_fh.WriteString(line+"\n")
					if (gt_proband == gt_brother && gt_brother != "./.") {	// proband matched brother ...
						vcf_line_counter_proband_match_brother++
						vcf_out_match_brother_fh.WriteString(line+"\n")
						if (((gt_proband_1 == gt_son_1 || gt_proband_2 == gt_son_2) && gt_proband != gt_son) || ((gt_proband_1 == gt_daughter_1 || gt_proband_2 == gt_daughter_2) && gt_proband != gt_daughter) && gt_son != "./." && gt_daughter != "./.") {	// kids het ...
							vcf_line_counter_proband_not_match_kids++
							vcf_out_not_kids_fh.WriteString(line+"\n")
							//println("## "+line)
							//println("gt_proband: "+gt_proband)
							//println("gt_brother: "+gt_brother)
							//println("gt_son: "+gt_son)
							//println("gt_daughter: "+gt_daughter)
							info := fields[7]
							//println("info: "+info)
		
							re_gno_af_all_res := re_gno_af_all.FindStringSubmatch(info)
						
							//fmt.Printf("%v", res)
							if (len(re_gno_af_all_res) >= 1) {
								//println(""+re_gno_af_all_res[1])
								if gno_af_all, err := strconv.ParseFloat(re_gno_af_all_res[1], 32); err == nil {
									if (gno_af_all < 0.001) {
										vcf_line_below_freq_counter++
										//println(line)
										vcf_out_fh.WriteString(line+"\n")
										//println(gno_af_all)
										//println("## "+line)
										//println("gt_proband: "+gt_proband)
										//println("gt_brother: "+gt_brother)
										//println("gt_son: "+gt_son)
										//println("gt_daughter: "+gt_daughter)
										//info := fields[7]
										//println("info: "+info)
									}
								}
							}
						} else if (gt_proband != gt_son && gt_proband != gt_daughter) {	// 
							//fmt.Println(line+"\n")
						}
					}
				}

			}
		} else {
			//println(line)
			vcf_out_only_pro_fh.WriteString(line+"\n")
			vcf_out_pro_hom_fh.WriteString(line+"\n")
			vcf_out_match_brother_fh.WriteString(line+"\n")
			vcf_out_not_kids_fh.WriteString(line+"\n")
			vcf_out_fh.WriteString(line+"\n")
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	
	vcf_in_fh.Close()
	vcf_out_fh.Close()
	vcf_out_only_pro_fh.Close()
	vcf_out_pro_hom_fh.Close()
	vcf_out_match_brother_fh.Close()
	vcf_out_not_kids_fh.Close()

	fmt.Println(fmt.Sprintf("vcf_line_counter: %d", vcf_line_counter))
	fmt.Println(fmt.Sprintf("vcf_line_counter_proband_has_variant: %d", vcf_line_counter_proband_has_variant))
	fmt.Println(fmt.Sprintf("vcf_line_counter_proband_hom: %d", vcf_line_counter_proband_hom))
	fmt.Println(fmt.Sprintf("vcf_line_counter_proband_match_brother: %d", vcf_line_counter_proband_match_brother))
	fmt.Println(fmt.Sprintf("vcf_line_counter_proband_not_match_kids: %d", vcf_line_counter_proband_not_match_kids))
	fmt.Println(fmt.Sprintf("vcf_line_below_freq_counter: %d", vcf_line_below_freq_counter))

	return
}




// Exists reports whether the named file or directory exists.
func exists(name string) bool {
    if _, err := os.Stat(name); err != nil {
    if os.IsNotExist(err) {
                return false
            }
    }
    return true
}